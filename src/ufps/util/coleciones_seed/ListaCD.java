/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

/**
 *
 * @author madarme
 */
public class ListaCD<T> {
    
    private NodoD<T> cabecera;
    private int tamano=0;
    
    
    public ListaCD<T> getMenores(T info)
    {
        ListaCD<T> l2=new ListaCD();
            
                // :)
        
        return l2;
    }
    
    
    
    public ListaCD() {
        this.cabecera=new NodoD();
        this.cabecera.setInfo(null);
        this.cabecera.setSiguiente(this.cabecera);
        this.cabecera.setAnterior(cabecera);
         
    }

    public int getTamano() {
        return tamano;
    }
    
    
    public void insertarInicio(T info)
    {
    NodoD<T> nuevo=new NodoD();
    nuevo.setInfo(info);
    nuevo.setSiguiente(this.cabecera.getSiguiente());
    //El anterior de nuevo nodo ES la cabecera
    nuevo.setAnterior(this.cabecera);
    //El siguiente de cabecera es el nuevo nodo
    this.cabecera.setSiguiente(nuevo);
    //El siguiente del nuevo nodo SU anterior ES el nuevo nodo
    nuevo.getSiguiente().setAnterior(nuevo);
    //Aumentar la cardinalidad
    this.tamano++;
    }
    
    
    public void insertarFin(T info)
    {
    NodoD<T> nuevo=new NodoD();
    nuevo.setInfo(info);
    //El anterior de nuevo es el anterior de cabecera
    nuevo.setAnterior(this.cabecera.getAnterior());
    //El siguiente de nuevo es cabecera
    nuevo.setSiguiente(this.cabecera);
    //El anterior de cabecera su siguiente ES nuevo nodo
    this.cabecera.getAnterior().setSiguiente(nuevo);
    //El anterior de cabecera es ahora nuevo
    this.cabecera.setAnterior(nuevo);
    //Aumento cardinalidad
    this.tamano++;
    }
    
    public boolean esVacia()
    {
    // Método 1: tamano ==0 
    // Método 2:
    return this.cabecera==this.cabecera.getSiguiente() && this.cabecera==this.cabecera.getAnterior();
    }

    @Override
    public String toString() {
        String msg="ListaCD{";
        
        for (NodoD<T> x=this.cabecera.getSiguiente();x!=this.cabecera;x=x.getSiguiente())
            msg+=x.getInfo().toString()+"<-->";
        
        return msg+"}";
    }
    
    
    public T eliminar (int pos)
    {
      /*
        Comprobar que pos (posición) sea válida  >0 && < cardinalidad xxx
        Buscar el nodo actual dada la posición  getPos(…) xxxx
        Coloco nodo anterior = actual.getAnt() xxxx
        --->
        nodoAnt su siguiente Es el siguiente de nodo actual xxx
        El siguiente de nodo actual su anterior es nodoAnt
        Cardinilidad –
        desunir nodoActual (free(..))
        Retornar el info del nodo actual
        */  
      try{
          NodoD<T> nodoActual=this.getPos(pos);
          NodoD<T> nodoAnt=nodoActual.getAnterior();
          nodoAnt.setSiguiente(nodoActual.getSiguiente());
          nodoActual.getSiguiente().setAnterior(nodoAnt);
          this.tamano--;
          this.desUnir(nodoActual);
          return nodoActual.getInfo();
          
      }catch(Exception e)  
      {
          System.err.println(e.getMessage());
          return null;
      }
        
    }
    
    private void desUnir(NodoD<T> x)
    {
        x.setAnterior(x); //x.setAnt(null)
        x.setSiguiente(x);////x.setSig(null)
    }
    
    private NodoD<T> getPos(int pos) throws Exception
    {
        
        if(pos<0 || pos>=this.tamano)
            throw new Exception("La posición "+pos+" no es válida en la lista");
        NodoD<T> nodoPos=this.cabecera.getSiguiente();
        while(pos-->0)
            nodoPos=nodoPos.getSiguiente();
        return nodoPos;
        
    }
    private NodoD<T> getPosOpti(int pos) throws Exception {
        //  Verificar que pos sea válida pos< cardinalidad y no es vacía y pos>=0
        //Es vacio sobra...
        if (this.esVacia() || pos < 0 || pos >= this.tamano) {
            throw new Exception("La posición " + pos + " No es válida en la lista");
        }
        //Contribución de Carlos Eduardo Contreras Mendoza
       
        if (pos == this.tamano - 1) {
            return this.cabecera.getAnterior();
        }
       
        
        
        //EL METODO SE PRUEBA EL LA CLASE TestGetPostListaCD DE LA VISTA
        NodoD<T> nodoPos;
        if (pos >= this.tamano / 2) {
            pos = this.tamano- 1 - pos;
            nodoPos = this.cabecera.getAnterior();
            while (pos-- > 0) {
                nodoPos = nodoPos.getAnterior();
            }
        } else {
            nodoPos = this.cabecera.getSiguiente();
            while (pos-- > 0) {
                nodoPos = nodoPos.getSiguiente();
            }
        }
        return nodoPos;

    }
    
    public T get(int pos)
    {
    
        try {
            return this.getPos(pos).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }
    
    
    public void set(int pos, T infoNuevo)
    {
    
        
        try {
                this.getPos(pos).setInfo(infoNuevo);
        } catch (Exception ex) {
                System.err.println(ex.getMessage());
            
        }
    }
    
    
    /*
        Corta los nodos de posición inical a pos final y pasa estos nodos a la lista l2
    
    Condiciones:
    1. posInicial<=posfinal
    2. NO SE DEBEN CREAR NODOS, SOLO SE PUEDEN REFERENCIAR ( NO USAR INSERFIN O INSRINICIO)
    
    Ejemplo: l1=<12,16,17,11,13,10> , l2=l1.cortar(2,4) 
             l1=<12,16,10> y l2=<17,11,13>
    */
    public ListaCD<T> cortar(int posInicial, int posFinal)
    {
    
        ListaCD<T> l2=new ListaCD<T>();
        
        if(posInicial<=posFinal){
            try {
                int cardinalidad2 = posFinal-posInicial +1;
                NodoD<T> nodoInicial = this.getPos(posInicial);
                NodoD<T> nodoFinal = this.getPos(posFinal);
                nodoFinal.getSiguiente().setAnterior(nodoInicial.getAnterior());
                nodoInicial.getAnterior().setSiguiente(nodoFinal.getSiguiente());
                nodoFinal.setSiguiente(l2.cabecera);
                nodoInicial.setAnterior(l2.cabecera);
                l2.cabecera.setSiguiente(nodoInicial);
                l2.cabecera.setAnterior(nodoFinal);
                l2.tamano = cardinalidad2;
                this.tamano = this.tamano - cardinalidad2;
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                return l2;
            }
            
        }
        
        return l2;
    }
    
    
    public boolean esPalindrome() throws Exception{
        if(this.esVacia() )
            throw new Exception("La lista está vacia");
        
        NodoD<T> validador1=this.cabecera.getSiguiente();
        NodoD<T> validador2 = this.cabecera.getAnterior();
        //para 1, 2 y 3 elementos
        if(this.tamano<=3) 
            if(validador1.getInfo().equals(validador2.getInfo()))
                return true;
        //primera iteracion
        if(!validador1.getInfo().equals(validador2.getInfo()))
                return false;
        //siguientes iteraciones en n/2 hasta que finaliza
        for(int i=0;i<(int)((this.tamano-2)/2);i++){
            validador1 = validador1.getSiguiente();
            validador2= validador2.getAnterior();
            //retorna false cuando deja de ser palindromo
            if(!validador1.getInfo().equals(validador2.getInfo()))
                return false;
                    
        }
        return true;
    }
    
    public void pasarAlFinal(T info){
        NodoD<T> i=this.cabecera.getSiguiente();
        //For con indice para que al mover los nodos no termine antes el ciclo o para que no se vuelva un ciclo infinito
        for (int j=0;j<this.tamano;j=j+1){
            NodoD<T> nodoSiguiente = i.getSiguiente();
            NodoD<T> x = i;
            Comparable comparador=(Comparable)info;
            int rta=comparador.compareTo(x.getInfo());
            if(rta>=0){
                x.getSiguiente().setAnterior(x.getAnterior());
                x.getAnterior().setSiguiente(x.getSiguiente());
                x.setSiguiente(this.cabecera);
                x.setAnterior(this.cabecera.getAnterior());
                this.cabecera.getAnterior().setSiguiente(x);
                this.cabecera.setAnterior(x);
            }
            i=nodoSiguiente;
        }
    }
    
}
