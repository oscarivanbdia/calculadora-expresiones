/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

/**
 * Implementando una pila a través de una ListaCD
 * @author madar
 */
public class Pila<T> {
    
    private ListaCD<T> tope;

    public Pila() {
        
        this.tope=new ListaCD();
    }
    
    public void push(T elemento)
    {
        this.tope.insertarInicio(elemento);
    }
    
    public T pop()
    {
        return this.tope.eliminar(0);
    }
    
    public boolean esVacia()
    {
        return this.tope.esVacia();
    }
    
    public int getTamano()
    {
        return this.tope.getTamano();
    }
    
    public T peek(){
        return this.tope.get(0);
    }
    
}
