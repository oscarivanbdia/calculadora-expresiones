/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista.ejercicios;

import java.util.Scanner;
import ufps.util.coleciones_seed.Pila;

/**
 *
 * @author madar
 */
public class Evaluador_parentesis {
    
    public static void main(String[] args) {
        String cadena;
        System.out.println("Digite una cadena con ()");
        Scanner teclado = new Scanner(System.in);
        cadena=teclado.nextLine();
        System.out.println("Evaluando sus paréntesis: "+evaluador(cadena));
        
    }
    
    
    private static boolean evaluador(String cadena)
    {
        Pila<Character> pila = new Pila();
        // :)
        char arregloExp[] = cadena.toCharArray();
        for (int i=0; i<arregloExp.length;i++)
        {
            if(arregloExp[i]=='(')
                pila.push('(');
            else if(arregloExp[i]==')'){
                if(!pila.esVacia())
                    pila.pop();
                else
                    pila.push(arregloExp[i]);
            }
            
        }
        return pila.esVacia();
        
    }
}
