/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista.ejercicios;

import java.util.Scanner;
import ufps.util.coleciones_seed.Pila;

/**
 *
 * @author RYZEN
 */
public class EvaluadorExpresion {
    private static String operador = "^*/+-(";
    public static void main(String[] args) {
        String cadena;//1*(2+3-(4/5^6)-7)-8 ?-10.000256
        System.out.println("Digite una expresión aritmética");
        Scanner teclado = new Scanner(System.in);
        cadena=teclado.nextLine();
        if (evaluadorParentesis(cadena)){
        String expPost= postFija(cadena);
        System.out.println("Ecuacion en postfijo: "+expPost);
        //4*(5-2)/(1*9^3)
        String expPre= preFija(cadena);
        System.out.println("Ecuacion en prefijo: "+expPre);
            System.out.println("PROCESO DE EVALUACIÓN ARITMÉTICA");
        System.out.println("Ecuacion evaluada: "+postFijaRta(expPost));
        }
        else
            System.out.println("Parentesis no balanceados");
    }
    
    
    public static boolean evaluadorParentesis(String cadena)
    {
        Pila<Character> pila = new Pila();
        // :)
        char arregloExp[] = cadena.toCharArray();
        for (int i=0; i<arregloExp.length;i++)
        {
            if(arregloExp[i]=='(')
                pila.push('(');
            else if(arregloExp[i]==')'){
                if(!pila.esVacia())
                    pila.pop();
                else
                    pila.push(arregloExp[i]);
            }
            
        }
        return pila.esVacia();
        
    }

    public static String postFija(String cadena){
        Pila<Character> pila = new Pila();
        char[] expsion = new char[cadena.length()];
        char[] cad = new char[cadena.length()];
        cad = cadena.toCharArray();
        String expPost= new String("");
        int j=0;
        for(int i=0;i<cadena.length();i++){
            char x= cadena.charAt(i);
//Si es un operando, pasarlo a la expresión postfija
            
            String y= String.valueOf(x);
            if(x>=48&&x<=57){
                expPost += x;
                j++;
            }
//Si es operador:
            else if (operador.contains(y)){
                if(pila.esVacia())
                    pila.push(x);
                else{
                    while(!pila.esVacia()&&(prdadFuera(x)<=prdadDentro(pila.peek()))){
                        expPost +=pila.pop();
                        j++;
                    }
                    pila.push(x);
//                    if(prdadFuera(x)>prdadDentro(pila.peek()))
//                        pila.push(x);
//                    else{
//                        expsion[j]=pila.pop();
//                        j++;
//                    }
                
                }
                    
            }

//Si es paréntesis derecho:
            else if(x==')'){
                
                while(pila.peek()!='('){
                expPost +=pila.pop();
                }
                pila.pop();
            }
            
           

        }
        //Si quedan elementos en la pila, pasarlos a la expresión postfija.
        while(!pila.esVacia()){
                    expPost +=pila.pop();
                    j++;
                }
        
        
//        for(int i=0;i<expsion.length&&expsion[i]!="";i++){
//            expPost += expsion[i];
//        }
        return expPost;
    }
    private static int prdadDentro(char x){
        switch (x) {
            case '^':
                return 3;
            case '*':
            case '/':
                return 2;
            case '+':
            case '-':
                return 1;
            default:
                return 0;
        }
        
    }
    private static int prdadFuera(char x){
        switch (x) {
            case '^':
                return 4;
            case '*':
            case '/':
                return 2;
            case '+':
            case '-':
                return 1;
            default:
                return 5;
        }
       
    }
    
    /**
     *
     * @param cadena
     * @return expresion evaluada en postfijo
     */
    public static double postFijaRta(String expsion){
        Pila<Double> operandos = new Pila();
        for(int i=0;i<expsion.length();i++){
           char x= expsion.charAt(i);
//Si es un operando, pasarlo a la pila
            String y= String.valueOf(x);
            System.out.println(y);
            if(x>=48&&x<=57){
                int num = (int) x;
                num = num - 48;
                operandos.push((double)num);
                
            }
            else if (operador.contains(y)){
                double z =0;
                double b =  operandos.pop();
                if((x=='-')&&(operandos.esVacia()))
                {
                    z = b * (-1);
                }
                else
                {
                    double a =  operandos.pop();
                
                switch (y){
                    case "^":
                         z = Math.pow(a,b);
                         break;
                    case "/":
                        z = (double) a/b;
                        break;
                    case "*":
                        z=a*b;
                        break;
                    case "+":
                        z=a+b;
                        break;
                    case "-":
                        z=a-b;
                        break;
                    }
                }
                System.out.println(z);
                operandos.push(z);
            }
        }
        return operandos.peek();
    }
    
    
    
    
    public static String preFija(String cadena) 
    { 
     
        Pila<Character> operadores = new Pila<Character>(); 
 
        Pila<String> operandos = new Pila<String>(); 
  
        for (int i = 0; i < cadena.length(); i++)  
        { 
            char x= cadena.charAt(i);
            String y= String.valueOf(x);
         
            if (x == '(')  
            { 
                operadores.push(x); 
            } 

            else if (x == ')')  
            { 
                while (!operadores.esVacia() &&  
                        operadores.peek() != '(')  
                { 
                    // operando 1 
                    String op1 = operandos.peek(); 
                    operandos.pop(); 
  
                    // operando 2 
                    String op2 = operandos.peek(); 
                    operandos.pop(); 
  
                    // operador 
                    char op = operadores.peek(); 
                    operadores.pop(); 
   
                    String tmp = op + op2 + op1; 
                    operandos.push(tmp); 
                } 
  
                operadores.pop(); 
            } 
  
            else if (!operador.contains(y))  
            { 
                operandos.push(x + ""); 
            } 
  
            else 
            { 
                while (!operadores.esVacia() &&  
                        prdadDentro(x) <=  
                        prdadDentro(operadores.peek()))  
                { 
  
                    String op1 = operandos.peek(); 
                    operandos.pop(); 
  
                    String op2 = operandos.peek(); 
                    operandos.pop(); 
  
                    char op = operadores.peek(); 
                    operadores.pop(); 
  
                    String tmp = op + op2 + op1; 
                    operandos.push(tmp); 
                } 
  
                operadores.push(x); 
            } 
        } 
  
        while (!operadores.esVacia())  
        { 
            String op1 = operandos.peek(); 
            operandos.pop(); 
  
            String op2 = operandos.peek(); 
            operandos.pop(); 
  
            char op = operadores.peek(); 
            operadores.pop(); 
        
            String tmp = op + op2 + op1; 
            operandos.push(tmp); 
        } 
  
        return operandos.peek(); 
    } 
}
