/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.*;

/**
 *
 * @author madar
 */
public class TestPilas_Colas {
    
    public static void main(String[] args) {
        Pila<Persona> p=new Pila();
        Cola<Persona> c=new Cola();
        /*
        Creando personas: 
        */
        Persona uno=new Persona(1, "diego");
        Persona dos=new Persona(2, "daniela");
        Persona tres=new Persona(3, "jairo");
        Persona cuatro=new Persona(4, "brayan");
        
        /**
         * Insertar estos elementos en una Pila:
         */
        
        p.push(uno);
        p.push(dos);
        p.push(tres);
        p.push(cuatro);
        
        System.out.println("MI pila tiene :"+p.getTamano()+" elementos");
        
        while(!p.esVacia())
            System.out.print(p.pop().toString()+"\t");
        
        System.out.println("\n MI pila tiene :"+p.getTamano()+" elementos");
        
        
        c.enColar(uno);
        c.enColar(dos);
        c.enColar(tres);
        c.enColar(cuatro);
        
        
        System.out.println("MI cola tiene :"+c.getTamano()+" elementos");
        
        while(!c.esVacia())
            System.out.print(c.deColar().toString()+"\t");
        
        System.out.println("\n MI Cola tiene :"+c.getTamano()+" elementos");
        
    }
}
